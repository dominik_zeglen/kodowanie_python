debug = False

def shiftBinaryPolynomial(poly, n, k):
	output_poly = binaryPolynomial(n)
	for i in range(abs(k)):
		for j in range(n):
			output_poly[j] = poly[(j + k / abs(k)) % n]

	return output_poly
	
def copyArray(array):
	output_array = []
	
	for i in range(len(array)):
		output_array.append(array[i])
	
	return output_array

def reverseBinaryPolynomial(poly):
	output_poly = []
	for i in range(len(poly)):
		output_poly.append(poly[len(poly) - 1 - i])
	
	return output_poly 

def addMatrices(matrix_a, matrix_b):
	if len(matrix_a) == len(matrix_b):
		if len(matrix_a[0]) == len(matrix_b[0]):
			matrix= []
			for i in range(len(matrix_a)):
				matrix.append([])
				for j in range(len(matrix_a[0])):
					matrix[i].append(matrix_a[i][j] + matrix_b[i][j])
			return matrix
		else:
			return -1
	else:
		return -1
			
def matrixModulo(matrix, modulo):
	for i in range(len(matrix)):
		for j in range(len(matrix[0])):
			matrix[i][j] = matrix[i][j] % modulo
	
	return matrix

def transposeMatrix(matrix):
	matrix_output = []
	for i in range(len(matrix[0])):
		matrix_output.append([])
		for j in range(len(matrix)):
			matrix_output[i].append(matrix[j][i])
	return matrix_output

def printMatrix(matrix):
	for i in range(len(matrix)):
		for j in range(len(matrix[0])):
			print str(str(matrix[i][j]) + "\t"),
		print

def identityMatrix(n):
	matrix = []
	for i in range(n):
		matrix.append([])
		for j in range(n):
			if i == j:
				matrix[i].append(1)
			else:
				matrix[i].append(0)
			
	return matrix

def multiplyMatrices(matrix_a, matrix_b):
	matrix = []
	if len(matrix_a) == len(matrix_b[0]):
		for i in range(len(matrix_a)):
			matrix.append([])
			for j in range(len(matrix_a)):
				matrix[i].append(0)
				for r in range(len(matrix_a)):
					matrix[i][j] += (matrix_a[i][r] * matrix_b[r][j])
		return matrix
		
	else:
		return -1
	
def printBinaryPolynomial(poly):
	output = ""
	for i in range(len(poly)):
		output += str(poly[len(poly) - i - 1])
	
	print output

def binaryPolynomial(a):
	poly = []
	for i in range(a):
		poly.append(0)
	
	poly.append(1)
	
	return poly

def cutLastCoefficients(poly, n = -1):
	if debug:
		print "Cutting last coefficients"
	if n < 0:
		while not poly[len(poly) - 1] and len(poly) > 1:
			poly.pop()
	else:
		for i in range(n):
			if not poly[len(poly) - 1] and len(poly) > 1:
				poly.pop()
	return poly

def addBinaryPolynomials(poly_a, poly_b, n = -1):
	if debug:
		print "Adding polynomials"
	if len(poly_a) >= len(poly_b):
		poly = []
		
		for i in range(len(poly_a)):
			poly.append(poly_a[i])
		
		for i in range(len(poly_b)):
			poly[i] = poly[i] ^ poly_b[i]
	else:
		return addBinaryPolynomials(poly_b, poly_a)
	
	return cutLastCoefficients(poly, n)

def multiplyBinaryPolynomials(poly_a, poly_b, n = -1):
	if debug:
		print "Multiplying polynomials"
	poly = []
	for i in range(len(poly_a)):
		if poly_a[i]:
			for j in range(len(poly_b)):
				if poly_b[j]:
					if debug:
						print poly, "+", binaryPolynomial(j)
					poly = addBinaryPolynomials(poly, binaryPolynomial(j + i))
				else:
					if debug:
						print poly, "+", []
					
	return cutLastCoefficients(poly, n)

def divideBinaryPolynomials(poly_a, poly_b):
	if len(poly_a) >= len(poly_b):
		poly = []
		temp_poly = copyArray(poly_a)
		
		while(len(temp_poly) >= len(poly_b)):
			if debug:
				print "Before,", temp_poly
			poly.append(temp_poly[len(temp_poly) - 1])
			if not temp_poly[-1]:
				poly_c = [0]
			else:
				poly_c = multiplyBinaryPolynomials(poly_b, binaryPolynomial(len(temp_poly) - len(poly_b)))
			if debug:
				print "Divisor,", poly_c
			temp_poly = addBinaryPolynomials(temp_poly, poly_c, 1)
			if debug:
				print "After,", temp_poly
		return reverseBinaryPolynomial(poly), cutLastCoefficients(temp_poly)
