#	Auxiliary functions

from Algebra import *

def getHammingWeight(code):
	weight = 0
	
	for i in range(len(code)):
		if code[i]:
			weight += 1
	
	return weight

#	Coding / decoding functions

def coder(data, n, k, g):
	code = multiplyBinaryPolynomials(binaryPolynomial(n - k), data)
	code = divideBinaryPolynomials(code, g)[1]
	code = addBinaryPolynomials(multiplyBinaryPolynomials(data, binaryPolynomial(n - k)), code)
	return code
	
def decoder(code, n, k, g):
	try:
		for i in range(n):
			code_original = copyArray(code)
			if(getHammingWeight(divideBinaryPolynomials(code, g)[1]) <= 2):
				code = addBinaryPolynomials(code, divideBinaryPolynomials(code, g)[1])
				data = divideBinaryPolynomials(code, binaryPolynomial(n - k))[0]
				syndrome = addBinaryPolynomials(coder(data, n, k, g), code_original)
		
				return data, syndrome
			else:
				code = shiftBinaryPolynomial(code, n, 1)
	except:
		print "Error"
		return [0], [0]
	print "This error cannot be corrected"
	
#	Main function	

n = 21
k = 12

g = [0]
g = addBinaryPolynomials(g, binaryPolynomial(9))
g = addBinaryPolynomials(g, binaryPolynomial(6))
g = addBinaryPolynomials(g, binaryPolynomial(0))

data = [0]

while(True):
	x = input("Information Hamming weight: ")
	for i in range(x):
		data = addBinaryPolynomials(data, binaryPolynomial(input("Set information bit, from 0 to 12: ")))
	
	code = coder(data, n, k, g)
	rcode = coder(data, n, k, g)
	x = input("Error Hamming weight: ")
	for i in range(x):
		rcode = addBinaryPolynomials(rcode, binaryPolynomial(input("Set error bit: ")))

	print "generator polynomial"
	printBinaryPolynomial(g)
	print "sent data"
	printBinaryPolynomial(data)
	print "sent code"
	printBinaryPolynomial(code)
	print "received code"
	printBinaryPolynomial(rcode)
	print "received data"
	printBinaryPolynomial(decoder(rcode, n, k, g)[0])
	print "received syndrome"
	printBinaryPolynomial(decoder(rcode, n, k, g)[1])
	print 
